#!/usr/bin/env python

import argparse, sys
import file_archive.create as mod_create
import file_archive.common as mod_common
import file_archive.config as mod_config


epilog_text = '''\
Frequent Commands:
  create    Create a new Archive
  list      List all of your Archives
  verify    Verify one or more Archives
  
Behind The Scenes:
  check-config    Validate the contents of your config file
  get-hash        Get the SHA3-256 hash value for one or more files'''


def cmd_create():
    result = mod_create.create_archive()
    if result:
        print('All done creating Archive.')


def cmd_list():
    print('Listing!')


def cmd_verify():
    print('Verifying!')


def cmd_check_config():
    conf = mod_config.load_config()
    print(conf)


def cmd_get_hash():
    '''
    Get the SHA3-256 value for one or more files.
    TODO: Print the results as base-32 values.
    '''
    parser = argparse.ArgumentParser(
        description='File Archive Command Line Client')
    parser.add_argument(
        'command', nargs='+',
        help='The file(s) to get hashes for.')
    args = parser.parse_args()
    command = args.command
    assert(command[0].lower() == 'get-hash')
    file_names = args.command[1:]
    if not file_names:
        print('{0}: error: one or more file names are required'.format(
            sys.argv[0]))
        return False
    for fname in file_names:
        result = mod_common.get_sha3_256_for_file(fname)
        print(result)
        print(len(result))


def run_without_command(show_help):
    '''
    The command line options vary per command. Before we roll our own arg
    parser, see if one of the commands can take care of the job instead.
    '''
    parser = argparse.ArgumentParser(
        description='File Archive Command Line Client', 
        formatter_class=argparse.RawDescriptionHelpFormatter,
        epilog=epilog_text)
    parser.add_argument('--version', action='store_true',
        help='Print the version number and exit')
    args = parser.parse_args()
    # If they want the version, just print that and quit.
    if args.version:
        print('file_archive_cli.py version 0.1')
        return True
    else:
        parser.print_help()


def main():
    '''
    Depending on the command, dispatch to the appropriate function, which will
    do the appropriate arg parsing, since we have a lot going on here.
    Return true if everything ran successfully.
    '''
    dispatch = {
       'create':       cmd_create,
       'list':         cmd_list,
       'verify':       cmd_verify,
       'check-config': cmd_check_config,
       'get-hash':     cmd_get_hash}
    # First, check for a command.
    if len(sys.argv) >= 2:
        command  = sys.argv[1].lower().strip()
        cmd_to_run = dispatch.get(command, None)
        if cmd_to_run:
            cmd_to_run()
            return True
    # No luck? Go for the standard command line.
    run_without_command(show_help=True)
    return True


# And away we go.
if __name__ == '__main__':
    main()