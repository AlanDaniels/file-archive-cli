#!/usr/bin/env

'''
Stuff that's common across files.
'''

import base64, datetime, hashlib, os.path


# Constants
MANIFEST_VERSION  = '0.1Beta'
HASH_ALGORITHM    = 'SHA3-256'
LONG_STRING_BREAK = 80


def get_current_timestamp():
    '''Get the current time, in XXX format.'''
    result = datetime.datetime.utcnow().isoformat()
    return result


def get_mime_type_for_file(fname):
    '''
    For a given file name, get the mime type.
    TODO: Python "magic" could do this, but for now, let's avoid dependencies.
    '''
    if not os.path.isfile(fname):
        raise IOError('"{}" is not a file'.format(fname))
    lookup = {'.gif': 'image/gif',
              '.jpg': 'image/jpeg',
              '.png': 'image/png',
              '.pdf': 'application/pdf',
              '.mp4': 'video/mp4'}
    extn = os.path.splitext(fname)[-1].lower()
    result = lookup.get(extn, None)
    if not result:
        raise IOError('Cannot figure out mime type for file "{}"'.format(fname))
    return result


def get_sha3_256_for_file(fname):
    '''
    Get the SHA3-256 hash for a file, base32 encoded.
    '''
    if not os.path.isfile(fname):
        raise IOError('"{}" is not a file'.format(fname))
    hasher = hashlib.sha3_256()
    with open(fname, 'rb') as fp:
        hasher.update(fp.read())
    result = base64.b32encode(hasher.digest()).decode('utf-8')
    return result


def base32_encode_file(fname):
    '''
    For a thumbnail image, encode it such that we can add it to a JSON Manifest.
    Remember that base-32 encoding results in bytes, not a string, so we need to
    decode it to a string so that adding it to a JSON result doesn't break
    things. Break the line into multiple strings so that, if needed, we can open
    the resulting JSON Manifiest without an editor chocking on the contents. So,
    the result is a list rather than a single string.
    '''
    if not os.path.isfile(fname):
        raise IOError('"{}" is not a file'.format(fname))
    with open(fname, 'rb') as fp:
        one_line_bytes = base64.b32encode(fp.read())
    result = []
    one_line_str = one_line_bytes.decode('utf-8')
    while one_line_str:
        front        = one_line_str[:LONG_STRING_BREAK]
        one_line_str = one_line_str[LONG_STRING_BREAK:]
        result.append(front)
    return result
        


def run_unit_tests():
    pass


if __name__ == '__main__':
    run_unit_tests()