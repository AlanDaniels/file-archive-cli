#!/usr/bin/env python

'''
Code for creating File Archives.
'''

import argparse, datetime, glob, json, os.path, sys, zipfile
from collections import OrderedDict

import file_archive.common as mod_common
import file_archive.config as mod_config


# Load our config file.
conf = mod_config.load_config()


def abs_fname_to_rel_fname(base_dir, fname):
    '''Turn an absolute file name into a relative one.'''
    result = fname[len(base_dir):]
    while result.startswith(os.path.sep):
        result = result[1:]
    return result


def expand_wildcard_file_names(base_dir, file_names):
    '''
    Expand any wildcards in our list of file names.
    Also, sort the result for debugging.
    '''
    file_names = [os.path.join(base_dir, x) for x in file_names]
    results = []
    for fname in file_names:
        if '*' in fname:
            expanded = glob.glob(fname)
            if not expanded:
                raise IOError(
                    'Pattern "{}" does not expand to anything.'.format(fname))
            for expanded_fname in expanded:
                results.append(expanded_fname)
        else:
            results.append(fname)
    # Make sure all the files to add exist.
    for fname in results:
        if not os.path.isfile(fname):
            raise IOError('File "{}" does not exist'.format(fname))
    # Sort, and we're done.
    results.sort()
    return results


def build_file_list(base_dir, file_names):
    '''
    Build the JSON data we add to list the files in the payload.
    We have the file name, it's mime type, and size in bytes.
    '''
    result = []
    file_names = expand_wildcard_file_names(base_dir, file_names)
    for fname in file_names:
        rel_fname = abs_fname_to_rel_fname(base_dir, fname)
        mime_type = mod_common.get_mime_type_for_file(fname)
        file_size = os.path.getsize(fname)
        item = OrderedDict()
        item['file-name'] = rel_fname
        item['size-in-bytes'] = file_size
        item['mime-type'] = mime_type
        result.append(item)
    return result


def create_payload_file(base_dir, file_names):
    '''
    Take all the listed files and copy them to a temp zip file.
    It's a temp file, so just name it using the current date and time.
    Return the path of the new file.
    '''
    file_names = expand_wildcard_file_names(base_dir, file_names)
    # Compose the zip file name.
    now = datetime.datetime.now()
    timestamp = now.strftime('%Y%m%d-%H%M%S')
    payload_fname = 'payload-{}.zip'.format(timestamp)
    payload_path  = os.path.join(conf.temp_path, payload_fname)
    print('Creating zip file "{}"'.format(payload_path))
    # Add each file to the zip.
    with zipfile.ZipFile(payload_path, 'w', compression=zipfile.ZIP_DEFLATED,
                         allowZip64=True) as zf:
        for fname in file_names:
            rel_name = abs_fname_to_rel_fname(base_dir, fname)
            print('Adding "{0}"...'.format(fname))
            print('    As "{0}"...'.format(rel_name))
            zf.write(fname, rel_name)
    print(
        'All done creating zip file containing {} files'.format(
        len(file_names)))
    return payload_path


def parse_args():
    '''
    We have a lot of arg parsing going on. Do this separately.
    '''
    parser = argparse.ArgumentParser(
        description='File Archive Command Line Client')
    parser.add_argument('command',
        help='The "create" command, which you supplied.')
    parser.add_argument('-t', '--title', nargs=1,
        help='Title (required)')
    parser.add_argument('-d', '--description', nargs=1, dest='DESCR',
        help='Description (required, Markdown allowed)')
    parser.add_argument('-s', '--search', nargs='+',
        help='Search tags (one or more required)')
    parser.add_argument('-u', '--thumbnail', nargs=1,
        help='Thumbnail image (recommended)')
    parser.add_argument('-b', '--base', nargs=1,
        help='Base directory for files (required)')
    parser.add_argument('-f', '--file', nargs='+',
        help='File(s) to recursively add (required)')
    args = parser.parse_args()
    assert(args.command == 'create')
    # Get the list of files. Strange that this can be empty!
    file_names = args.file
    if not file_names:
        print('{0}: error: for create, one or more file names ' + 
              'are required'.format(sys.argv[0]))
        return None
    # All done.
    return args


def create_manifest_json_file(args, payload_fname):
    '''
    Create the manifest JSON file.
    This will be very simple; we won't be dealing with nulls, NaNs, etc.
    I'm using OrderedDicts here for the sake of debugging the output.
    '''
    conf = mod_config.load_config()
    payload_zip_base32 = mod_common.get_sha3_256_for_file(payload_fname)
    # Pluck useful stuff out of the args.
    base_dir   = args.base[0]
    file_names = args.file
    # Top level.
    manifest = OrderedDict()
    manifest['manifest-version'] = mod_common.MANIFEST_VERSION
    manifest['manifest-creation-date'] = mod_common.get_current_timestamp()
    # Author.
    manifest['author'] = OrderedDict()
    manifest['author']['name']   = conf.public_name
    manifest['author']['beacon'] = conf.beacon
    # Payload.
    hash_val = mod_common.get_sha3_256_for_file(payload_fname)
    manifest['payload'] = OrderedDict()
    manifest['payload']['size-in-bytes'] = os.path.getsize(payload_fname)
    manifest['payload']['hash-algorithm'] = mod_common.HASH_ALGORITHM
    manifest['payload']['hash-value'] = hash_val
    manifest['payload']['file-list'] = build_file_list(base_dir, file_names)
    # Thumbnail (if any). Adding this last, since it will be wordy.
    if args.thumbnail:
        thumbnail_fname = args.thumbnail[0]
        manifest['thumbnail'] = OrderedDict()
        manifest['thumbnail']['mime-type'] = (
            mod_common.get_mime_type_for_file(thumbnail_fname))
        manifest['thumbnail']['encoding'] = (
            mod_common.base32_encode_file(thumbnail_fname))
    result = json.dumps(manifest, indent=4)
    print(result)


def create_archive():
    '''
    Create an Archive out of one or more files.
    Both the payload zip file, and the manifest JSON, end up in the save path.
    Return true if successful.
    '''
    args = parse_args()
    if not args:
        return False
    base_dir   = args.base[0]
    file_names = args.file
    payload_fname  = create_payload_file(base_dir, file_names)
    create_manifest_json_file(args, payload_fname)
    return True


if __name__ == '__main__':
    print('No unit tests to run.')