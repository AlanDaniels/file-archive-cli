#!/usr/bin/env python

'''
Code for dealing with the config file.
'''

import json, os.path
from collections import namedtuple


Config = namedtuple('Config',
    ['beacon', 'public_name', 'save_path', 'temp_path'])


def find_config_path():
    '''
    Find where our config file is located.
    TODO: Right now, we're assuming a single config file.
    We might want to leave room for cascading files, like how Git does.
    '''
    config_path  = '.file_archive'
    config_fname = 'settings.json'
    # First, try the current directory.
    cur_json = os.path.join(os.curdir, config_fname)
    if os.path.isfile(cur_json):
        return cur_json
    # Then, try home.
    home_dir  = os.path.expanduser('~')
    home_json = os.path.join(home_dir, config_path, config_fname)
    if os.path.isfile(home_json):
        return home_json
    # Couldn't find the file.
    raise IOError('Cannot find config file "{}"'.format(config_fname))


def load_config(verbose=False):
    '''
    Load the config JSON and validate its contents.
    Return an object rather than the JSON blob itself.
    TODO: The "temp.path" is explicit for now, but use the OS setting later.
    '''
    config_fname = find_config_path()
    if verbose:
        print('Parsing config file file at "{0}"'.format(config_fname))
    with open(config_fname, mode='rt', encoding='utf-8') as fp:
        contents = json.load(fp)
    try:
        result = Config(
            beacon      = contents['beacon'],
            public_name = contents['public.name'],
            save_path   = contents['save.path'],
            temp_path   = contents['temp.path'])
    except KeyError as ex:
        raise IOError('Config file is missing entries.', ex)
    path_keys = ['save.path', 'temp.path']
    for path_key in path_keys:
        dname = contents[path_key]
        if not os.path.isdir(dname):
            raise IOError(
                'Path "{0}" in config file "{1}" does not exist'.format(
                    dname, config_fname))
    return result


# If run from the command line, just run a unit test.
if __name__ == '__main__':
    conf = load_config()
    print(conf)